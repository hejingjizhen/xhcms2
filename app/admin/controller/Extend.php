<?php 
/**
 *模型管理
*/

namespace app\admin\controller;

use xhadmin\service\admin\ExtendService;
use xhadmin\db\Extend as ExtendDb;

class Extend extends Admin {


	/*模型管理*/
	function index(){
		if (!$this->request->isAjax()){
			return $this->display('index');
		}else{
			$limit  = $this->request->post('limit', 0, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where['title'] = $this->request->param('title', '', 'strip_tags,trim');
			$where['table_name'] = $this->request->param('table_name', '', 'strip_tags,trim');
			$where['status'] = $this->request->param('status', '', 'strip_tags,trim');
			$where['type'] = $this->request->param('type', '', 'strip_tags,trim');

			$limit = ($page-1) * $limit.','.$limit;
			try{
				$res = ExtendService::pageList(formatWhere($where),$limit,$field,$orderby);
				$list = $res['list'];
			}catch(\Exception $e){
				exit($e->getMessage());
			}

			$data['rows']  = $list;
			$data['total'] = $res['count'];
			return json($data);
		}
	}

	/*修改排序、开关按钮操作 如果没有此类操作 可以删除该方法*/
	function updateExt(){
		$data = $this->request->post();
		if(!$data['extend_id']) $this->error('参数错误');
		try{
			ExtendDb::edit($data);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*添加*/
	function add(){
		if (!$this->request->isPost()){
			return $this->display('info');
		}else{
			$postField = 'title,table_name,status,type,action,sortid,orderby,is_post';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				ExtendService::add($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改*/
	function update(){
		if (!$this->request->isPost()){
			$extend_id = $this->request->get('extend_id','','intval');
			if(!$extend_id) $this->error('参数错误');
			$this->view->assign('info',checkData(ExtendDb::getInfo($extend_id)));
			return $this->display('info');
		}else{
			$postField = 'extend_id,title,table_name,status,type,action,sortid,orderby,is_post';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				ExtendService::update($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('extend_ids', '', 'strip_tags');
		if(!$idx) $this->error('参数错误');
		try{
			$where['extend_id'] = $idx;
			ExtendService::delete($where);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*查看数据*/
	function view(){
		$extend_id = $this->request->get('extend_id','','intval');
		if(!$extend_id) $this->error('参数错误');
		try{
			$this->view->assign('info',checkData(ExtendDb::getInfo($extend_id)));
			return $this->display('view');
		} catch (\Exception $e){
			$this->error($e->getMessage());
		}
	}



}

