<?php 
/**
 *会员管理
*/

namespace app\admin\controller;

use xhadmin\service\admin\MemberService;
use xhadmin\db\Member as MemberDb;
use think\facade\Cache;

class Member extends Admin {


	/*会员管理*/
	function index(){
		if (!$this->request->isAjax()){
			return $this->display('index');
		}else{
			$limit  = $this->request->post('limit', 0, 'intval');
			$offset = $this->request->post('offset', 0, 'intval');
			$page   = floor($offset / $limit) +1 ;

			$where['username'] = $this->request->param('username', '', 'strip_tags,trim');
			$where['sex'] = $this->request->param('sex', '', 'strip_tags,trim');
			$where['mobile'] = $this->request->param('mobile', '', 'strip_tags,trim');
			$where['email'] = $this->request->param('email', '', 'strip_tags,trim');
			$where['headimg'] = $this->request->param('headimg', '', 'strip_tags,trim');
			$where['status'] = $this->request->param('status', '', 'strip_tags,trim');
			$where['province'] = $this->request->param('province', '', 'strip_tags');;
			$where['city'] = $this->request->param('city', '', 'strip_tags');;
			$where['district'] = $this->request->param('district', '', 'strip_tags');;

			$create_time_start = $this->request->param('create_time_start', '', 'strip_tags');
			$create_time_end = $this->request->param('create_time_end', '', 'strip_tags');

			$where['create_time'] = \xhadmin\CommonService::getTimeWhere($create_time_start,$create_time_end);

			$limit = ($page-1) * $limit.','.$limit;
			try{
				$res = MemberService::pageList(formatWhere($where),$limit,$field,$orderby);
				$list = $res['list'];
			}catch(\Exception $e){
				exit($e->getMessage());
			}

			$data['rows']  = $list;
			$data['total'] = $res['count'];
			return json($data);
		}
	}

	/*修改排序、开关按钮操作 如果没有此类操作 可以删除该方法*/
	function updateExt(){
		$data = $this->request->post();
		if(!$data['id']) $this->error('参数错误');
		try{
			MemberDb::edit($data);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*添加*/
	function add(){
		if (!$this->request->isPost()){
			return $this->display('add');
		}else{
			$postField = 'username,sex,mobile,email,headimg,amount,status,province,city,district,password,tags,create_time';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				MemberService::add($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'添加成功']);
		}
	}

	/*修改*/
	function update(){
		if (!$this->request->isPost()){
			$id = $this->request->get('id','','intval');
			if(!$id) $this->error('参数错误');
			$this->view->assign('info',checkData(MemberDb::getInfo($id)));
			return $this->display('update');
		}else{
			$postField = 'id,username,sex,mobile,email,headimg,amount,status,province,city,district,tags,create_time';
			$data = $this->request->only(explode(',',$postField),'post');
			try {
				MemberService::update($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'修改成功']);
		}
	}

	/*删除*/
	function delete(){
		$idx =  $this->request->post('ids', '', 'strip_tags');
		if(!$idx) $this->error('参数错误');
		try{
			$where['id'] = explode(',',$idx);
			MemberService::delete($where);
		}catch(\Exception $e){
			$this->error($e->getMessage());
		}
		return json(['status'=>'00','msg'=>'操作成功']);
	}

	/*查看数据*/
	function view(){
		$id = $this->request->get('id','','intval');
		if(!$id) $this->error('参数错误');
		try{
			$this->view->assign('info',checkData(MemberDb::getInfo($id)));
			return $this->display('view');
		} catch (\Exception $e){
			$this->error($e->getMessage());
		}
	}

	/*导出*/
	function dumpData(){
		$where['username'] = $this->request->param('username', '', 'strip_tags,trim');
		$where['sex'] = $this->request->param('sex', '', 'strip_tags,trim');
		$where['mobile'] = $this->request->param('mobile', '', 'strip_tags,trim');
		$where['email'] = $this->request->param('email', '', 'strip_tags,trim');
		$where['headimg'] = $this->request->param('headimg', '', 'strip_tags,trim');
		$where['status'] = $this->request->param('status', '', 'strip_tags,trim');
		$where['province'] = $this->request->param('province', '', 'strip_tags');;
		$where['city'] = $this->request->param('city', '', 'strip_tags');;
		$where['district'] = $this->request->param('district', '', 'strip_tags');;

		$create_time_start = $this->request->param('create_time_start', '', 'strip_tags');
		$create_time_end = $this->request->param('create_time_end', '', 'strip_tags');
		$where['create_time'] = \xhadmin\CommonService::getTimeWhere($create_time_start,$create_time_end);

		$orderby = '';

		try {
			//此处读取前端传过来的 表格勾选的显示字段
			$fieldInfo = [];
			for($j=0; $j<100;$j++){
				$fieldInfo[] = $this->request->param($j);
			}
			$fieldInfo = \xhadmin\CommonService::filterEmptyArray($fieldInfo);
			$res = MemberService::dumpData(formatWhere($where),$orderby,array_unique($fieldInfo));
		} catch (\Exception $e) {
			$this->error($e->getMessage());
		}
	}

	/*导入*/
	function import(){
		if ($this->request->isPost()) {
			try{
				$key = 'Member';
				$result = \xhadmin\CommonService::importData($key);
				if (count($result) > 0) {
					Cache::set($key,$result,3600);
					return redirect('startImport');
				} else{
					$this->error('内容格式有误！');
				}
			}catch(\Exception $e){
				$this->error($e->getMessage());
			}
		}else {
			return $this->display('base/importData');
		}
	}

	//开始导入
	function startImport(){
		if(!$this->request->isPost()) {
			return $this->display('base/startImport');
		}else{
			$p = $this->request->post('p', '', 'intval'); 
			$data = Cache::get('Member');
			$export_per_num = config('my.export_per_num') ? config('my.export_per_num') : 50;
			$num = ceil((count($data)-1)/$export_per_num);
			if($data){
				$start = $p == 1 ? 2 : ($p-1) * $export_per_num + 1;
				if($data[$start]){
					$dt['percent'] = ceil(($p)/$num*100);
					try{
						for($i=1; $i<=$export_per_num; $i++ ){
						//根据中文名称来读取字段名称
							if($data[$i + ($p-1)*$export_per_num]){
								foreach($data[1] as $key=>$val){
									$fieldInfo = \app\admin\db\Field::getWhereInfo(['name'=>$val,'menu_id'=>526]);
									if($val && $fieldInfo){
									$d[$fieldInfo['field']] = $data[$i + ($p-1)*$export_per_num][$key];
									if($fieldInfo['type'] == 17){
										unset($d[$fieldInfo['field']]);
									}
									if(in_array($fieldInfo['type'],[7,12])){	//时间字段
										$d[$fieldInfo['field']] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($data[$i + ($p-1)*$export_per_num][$key]);
									}
									if($fieldInfo['type'] == 5){	//密码字段
										$d[$fieldInfo['field']] = md5($data[$i + ($p-1)*$export_per_num][$key].config('my.password_secrect'));
									}
									if($fieldInfo['type'] == 17){	//三级联动字段
										$arrTitle = explode('|',$fieldInfo['field']);
										$arrValue = explode('-',$data[$i + ($p-1)*$export_per_num][$key]);
										if($arrTitle && $arrValue){
											foreach($arrTitle as $k=>$v){
												$d[$v] = $arrValue[$k];
											}
										}
									}
									if(in_array($fieldInfo['type'],[2,3,23])){	//下拉，单选，开关按钮
										$d[$fieldInfo['field']] = getFieldName($data[$i + ($p-1)*$export_per_num][$key],$fieldInfo['config']);
									}
									}
								}
								$d['create_time'] = time();
								if(($i + ($p-1)*$export_per_num) > 1){
									MemberDb::createData($d);
								}
							}
						}
					}catch(\Exception $e){
						return json(['error'=>'01','msg'=>$e->getMessage()]);
					}
					return json(['error'=>'00','data'=>$dt]);
				}else{
					Cache::delete('Member');
					return json(['error'=>'10']);
				}
			}else{
				$this->error('当前没有数据');
			}
		}
	}
	/*数值加*/
	function recharge(){
		if (!$this->request->isPost()){
			$info['id'] = input('param.id','','strip_tags');
			$this->view->assign('info',$info);
			return $this->display('recharge');
		}else{
			$idx =  $this->request->post('id', '', 'strip_tags');
			$incData =  $this->request->post('amount', '', 'float');
			if(!$idx || !$incData) return json(['status'=>'01','msg'=>'参数错误']);
			try{
				$where['id'] = explode(',',$idx);
				$res = MemberDb::setInc($where,'amount',$incData);
			}catch(\Exception $e){
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'操作成功']);
		}
	}

	/*数值减*/
	function backRecharge(){
		if (!$this->request->isPost()){
			$info['id'] = input('param.id','','strip_tags');
			$this->view->assign('info',$info);
			return $this->display('backRecharge');
		}else{
			$idx =  $this->request->post('id', '', 'strip_tags');
			$decData =  $this->request->post('amount', '', 'float');
			if(!$idx || !$decData) return json(['status'=>'01','msg'=>'参数错误']);
			try{
				$where['id'] = $idx;
				$res = MemberDb::setDec($where,'amount',$decData);
			}catch(\Exception $e){
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'操作成功']);
		}
	}

	/*修改密码*/
	function updatePassword(){
		if (!$this->request->isPost()){
			$info['id'] = $this->request->get('id','','intval');
			$this->view->assign('info',$info);
			return $this->display('updatePassword');
		}else{
			$data = $this->request->post();
			try {
				$data['password'] = md5($data['password'].config('my.password_secrect'));
				MemberDb::edit($data);
			} catch (\Exception $e) {
				$this->error($e->getMessage());
			}
			return json(['status'=>'00','msg'=>'操作成功']);
		}
	}



}

